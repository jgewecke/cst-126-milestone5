<?php
// Project Name: Milestone5
// Project Version: 1.4
// Module Name: User Administrator
// Module Version: 1.4
// Programmer Name: Justin Gewecke
// Date: 7/26/2020
// Description: This module handles the admin portion of the website
// References: https://www.w3schools.com/php/php_mysql_insert.asp

    require_once('myfuncs.php');
    $link = dbConnect();

    $postID = $_POST['Data'];
    $row = getPostFromID($link, $postID);
    
    // Info about our post
    $text = $row["TEXT"];
    $title = $row["TITLE"];
    $authorID = $row["AUTHOR_ID"];
    $tag = $row["TAG"];
    
    // We need to get the author's first and last name again
    $sql = "SELECT ID, FIRST_NAME, LAST_NAME, EMAIL, USERNAME, PASSWORD FROM users WHERE ID='$authorID'";
    $result = mysqli_query($link, $sql);
    $row = $result->fetch_assoc();	// Read the Row from the Query
    $author = $row["FIRST_NAME"] . ' ' . $row["LAST_NAME"];
    
    
    // Display result
    echo nl2br("<h1>$title</h1> <h2>by $author</h2> <p>$text</p> <p id='tag'>Tag: $tag</p>");
    
    echo('<form action="modHandler.php" method="POST">
			<div class="group">
				<label>Edit Title</label>
			</div>
			<div class="group">
				<input type="text" name="Title" value="'.$title.'"/>
			</div>
			<div class="group">
				<label>Edit Tag</label>
			</div>
			<div class="group">
				<input type="text" name="Tag" value="'.$tag.'"/>
			</div>
			<div class="group">
				<label>Edit Text</label>
			</div>
			<div class="group">
				<textarea id="textInput" name="Text" rows="4" cols="50">'.$text.'</textarea>
			</div>
		
            <input type="hidden" name="Data" value="'.$postID.'"/>
			<input type="submit" name="Submit" value="Submit Changes" />
            <input type="submit" name="Delete" value="Delete Post" />
            <a href="viewPosts.php">Return to posts.</a>
		  </form>');
?>
