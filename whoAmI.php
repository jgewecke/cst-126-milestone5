<?php
// Project Name: Milestone5
// Project Version: 1.4
// Module Name: User Administrator
// Module Version: 1.4
// Programmer Name: Justin Gewecke
// Date: 7/26/2020
// Description: This module handles the admin portion of the website
// References: https://www.w3schools.com/php/php_mysql_insert.asp

require_once './myfuncs.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="UTF-8">
 <title>Who Am I</title>
</head>

<body>
 <h2>Hello My User ID Is: <?php echo " " . getUserId(); ?></h2><br>
 <a href="index.html">Go to main menu.</a>
</body>

</html>