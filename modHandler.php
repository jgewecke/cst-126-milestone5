<a href="viewPosts.php">Return to posts.</a>

<?php
// Project Name: Milestone5
// Project Version: 1.4
// Module Name: User Administrator
// Module Version: 1.4
// Programmer Name: Justin Gewecke
// Date: 7/26/2020
// Description: This module handles the admin portion of the website
// References: https://www.w3schools.com/php/php_mysql_insert.asp
// https://www.w3schools.com/sql/sql_delete.asp
// --NOTE--
// Post title is <=100 characters
// Post text is <= 2000 characters
// A chat filter is also added for some inappropriate words

    require_once('myfuncs.php');
    $link = dbConnect();
    
    $postID = $_POST["Data"];
    $title = $_POST["Title"]; 
    $text = $_POST["Text"];
    $tag = $_POST["Tag"];

    // Use this code if submit was pressed
    if (isset($_POST["Submit"]))
    {
        // Check for valid input /////////////////////////////////////////////////////////////
        // Simple filter
        $bannedWords = ['ass', 'fuck', 'bitch', 'cock', 'cum', 'cunt', 'dumbass', 'fag', 'faggot', 'goddamnit', 'goddamn', 'shit'];
        
        $approved = true;
        $titleSizeLimit = 100; // Number of characters allowed
        $textSizeLimit = 2000; // Number of characters allowed
               
        // Check for bad words
        foreach ($bannedWords as $badWord) {
            if (strpos(strtolower($text), $badWord) !== false) {
                echo("The word '" . $badWord . "' cannot be used in your text. Please remove it and try again. ");
                $approved = false;
            }
            
            if (strpos(strtolower($title), $badWord) !== false) {
                echo("The word '" . $badWord . "' cannot be used in your title. Please remove it and try again. ");
                $approved = false;
            }
        }
        
        // Check for empty fields
        if ($text == NULL)  { echo "The text field cannot be blank.\n"; $approved = false; }
        if ($title == NULL)  { echo "The title cannot be blank.\n"; $approved = false; }
        
        // Check for appropriate lengths
        if (strlen($title) > $titleSizeLimit) {
            echo "Your post title is too big. Please make it 100 characters or less";
            $approved = false;
        }
        if (strlen($text) > $textSizeLimit) {
            echo "Your blog post is too big. Please make it 2000 characters or less";
            $approved = false;
        }
        
        // Is the post approved?
        if ($approved) {
            echo "The post was updated! ";
        }
        else {
            echo "The post was not updated. ";
            exit;
        }
        ////////////////////////////////////////////////////////////////////////////////////////
        
        $sql = "UPDATE posts SET TITLE='$title', TEXT='$text', TAG='$tag' WHERE ID='$postID'";
        mysqli_query($link, $sql);

    }
    // Use this code if delete was pressed
    else if (isset($_POST["Delete"]))
    {
        $sql = "DELETE FROM posts WHERE ID='$postID'";
        mysqli_query($link, $sql);
        echo "The page has been deleted. ";
    }
    else
    {
        die("ERROR: Could not find submit or delete button");
    }
?>